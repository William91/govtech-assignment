# Use an official Nginx runtime as a parent image
FROM nginx:stable

# Copy the HTML, CSS, and JavaScript files into the container
COPY counter-project/index.html /usr/share/nginx/html/
COPY counter-project/js/index.js /usr/share/nginx/html/js/
COPY counter-project/css/index.css /usr/share/nginx/html/css/

# Expose port 80 for the Nginx server
EXPOSE 80

# Start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]