terraform {
  # If s3 bucket does not exist, comment out backend section first
  # After init,plan,apply, uncomment out backend section to transfer the backend from local to s3 bucket
  backend "s3" {
    bucket         = "govtech-tf-state"
    key            = "aws-backend-setup/terraform.tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "govtech-tf-state-locking"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_s3_bucket" "govtech_terraform_state" {
  bucket        = "govtech-tf-state"
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "terraform_bucket_versioning" {
  bucket = aws_s3_bucket.govtech_terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }

}

resource "aws_s3_bucket_server_side_encryption_configuration" "terraform_state_crypto_conf" {
  bucket = aws_s3_bucket.govtech_terraform_state.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "govtech-tf-state-locking"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}