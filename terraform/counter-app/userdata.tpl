#!/bin/bash
yum update -y &&
amazon-linux-extras install nginx1 -y &&

echo "server{
        listen  80;
        location /{
        root /home/ec2-user/counter-project;
        index index.html;
        }
}" > /etc/nginx/conf.d/counter.conf

systemctl start nginx
systemctl enable nginx
echo "Hello Nginx Demo" > /var/www/html/index.html
aws s3 sync s3://govtech-assignment/counter-project /home/ec2-user/counter-project --region ap-southeast-1
chmod o+x /home/ec2-user