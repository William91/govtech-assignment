region            = "ap-southeast-1"
instance_type     = "t2.micro"
image_id          = "ami-05ffd462e6e841c63"
desired_capacity  = 2
max_size          = 2
min_size          = 1
subnet_cidr_block = "172.31.48.0/20"
availability_zone = "ap-southeast-1a"