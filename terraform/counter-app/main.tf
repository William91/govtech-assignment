terraform {
  # Assumes s3 bucket and dynamo DB table already set up
  backend "s3" {
    bucket         = "govtech-tf-state"
    key            = "aws-backend-setup/terraform.tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "govtech-tf-state-locking"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.region
}

# Server Fleet A
resource "aws_launch_template" "server-fleet-a" {
  name_prefix            = "server-fleet-a"
  image_id               = var.image_id
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.alb.id]
  iam_instance_profile {
    name = aws_iam_instance_profile.server-fleet-a-profile.id
  }
  user_data = filebase64("userdata.tpl")
}

resource "aws_autoscaling_group" "bar" {
  name                = "server-fleet-a"
  vpc_zone_identifier = [aws_subnet.priv_subnet.id]
  desired_capacity    = var.desired_capacity
  max_size            = var.max_size
  min_size            = var.min_size
  target_group_arns   = [aws_lb_target_group.instances.arn]

  launch_template {
    id      = aws_launch_template.server-fleet-a.id
    version = "$Latest"
  }
}

# IAM role
data "aws_iam_role" "s3full" {
  name = "S3Full"
}

resource "aws_iam_instance_profile" "server-fleet-a-profile" {
  name = "server-fleet-a-profile"
  role = data.aws_iam_role.s3full.name
}

# Route Table for Private Subnet
resource "aws_route_table" "private" {
  vpc_id = data.aws_vpc.default.id

  # route {
  #   cidr_block     = "0.0.0.0/0"
  #   nat_gateway_id = "nat-0bb0bbc52ec261dba" # Existing NAT gateway
  # }

}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.priv_subnet.id
  route_table_id = aws_route_table.private.id
}

# Default VPC
data "aws_vpc" "default" {
  default = true
}

# Subnets
resource "aws_subnet" "priv_subnet" {
  vpc_id                  = data.aws_vpc.default.id
  cidr_block              = var.subnet_cidr_block
  map_public_ip_on_launch = false
  availability_zone       = var.availability_zone
}

data "aws_subnet_ids" "default_subnet" {
  vpc_id = data.aws_vpc.default.id
  filter {
    name   = "default-for-az"
    values = [true]
  }
}

# EC2 Instance Connect Endpoint (Error:  The provider hashicorp/aws does not support resource type "aws_ec2_instance_connect_endpoint".)

# resource "aws_ec2_instance_connect_endpoint" "ec2connect" {
#   subnet_id = aws_subnet.priv_subnet.id
# }

# VPC Endpoint
resource "aws_vpc_endpoint" "s3" {
  vpc_id          = data.aws_vpc.default.id
  service_name    = "com.amazonaws.ap-southeast-1.s3"
  route_table_ids = [aws_route_table.private.id]

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Id" : "PolicyForVPCE",
    "Statement" : [
      {
        "Sid" : "Access-to-specific-VPCE-only",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : [
          "s3:GetObject",
          "s3:ListBucket"
        ],
        "Resource" : [
          "arn:aws:s3:::govtech-assignment",
          "arn:aws:s3:::govtech-assignment/*"
        ]
        # ,
        # "Condition" : {
        #   "StringEquals" : {
        #     "aws:sourceVpce" : "vpce-0b1267a410bb228ec" # Update after creating this VPC Endpoint
        #   }
        # }
      },

      {
        "Sid" : "Amazon Linux AMI Repository Access",
        "Effect" : "Allow",
        "Principal" : "*",
        "Action" : "*",
        "Resource" : [
          "arn:aws:s3:::amazonlinux-2-repos-ap-southeast-1/*"
        ]
      }

    ]
  })
}

# Security groups

resource "aws_security_group" "alb" {
  name = "alb-security-group"
}

resource "aws_security_group_rule" "allow_alb_http_inbound" {
  type              = "ingress"
  security_group_id = aws_security_group.alb.id

  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

}

resource "aws_security_group_rule" "allow_alb_all_outbound" {
  type              = "egress"
  security_group_id = aws_security_group.alb.id

  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

}

# ALB
resource "aws_lb" "load_balancer" {
  name               = "counter-app-lb"
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.default_subnet.ids
  security_groups    = [aws_security_group.alb.id]

}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.load_balancer.arn

  port = 80

  protocol = "HTTP"

  # By default, return a simple 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_listener_rule" "instances" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.instances.arn
  }
}

resource "aws_lb_target_group" "instances" {
  name     = "counter-app-target-group1"
  port     = 80
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  lifecycle {
    create_before_destroy = true
  }
}

# S3 for counter files
resource "aws_s3_bucket" "govtech-assignment" {
  bucket        = "govtech-assignment"
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "govtech_assignment_bucket_versioning" {
  bucket = aws_s3_bucket.govtech-assignment.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "govtech_assignment_state_crypto_conf" {
  bucket = aws_s3_bucket.govtech-assignment.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}








# # S3 for terraform backend
resource "aws_s3_bucket" "govtech_terraform_state" {
  bucket        = "govtech-tf-state"
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "terraform_bucket_versioning" {
  bucket = aws_s3_bucket.govtech_terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "terraform_state_crypto_conf" {
  bucket = aws_s3_bucket.govtech_terraform_state.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# Dynamodb lock for terraform backend
resource "aws_dynamodb_table" "terraform_locks" {
  name         = "govtech-tf-state-locking"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}