# GovTech Assignment: Terraform, AWS, GitLab

Assumptions:

- NAT gateway is created without terraform(to delete when not in use) to reduce cost.
- A VPC Gateway endpoint is created to allow EC2 instances in private subnet to interact with the private S3 Bucket.

Improvements:

- Resources can be deployed in multiple availability zones for higher availability, to ensure the service is still up if one AZ goes down.
- Monitoring and alerting can be setup with AWS Cloudwatch for the infrastructure components
- In my GitLab pipeline, some improvements can be made:
    - validating the success of the refresh operation by querying AWS API or perform health checks
    - a rollback mechanism in case the refresh fails
    - logging and output in the job of the refresh operation


# GovTech Assignment: Kubernetes

Assumptions:

- Dockerfile to containerise counter-app with nginx. Docker installed locally for building dockerfile to create the image needed to deploy counter-app on kubernetes.
- MySQL storage will not persist once the cluster is shutdown, as it is hosted locally.
- Resources required to run the pod for this app is minimal, therefore configurations for resource request and limits are not set.
- The application is only allowed accesible locally. To expose the pods to the internet, further configuration is needed such as anIngress Controller/Resource etc.