#!/bin/bash

# Deploy mySQL
kubectl apply -f mysql-secret.yaml
kubectl apply -f mysql-pv.yaml
kubectl apply -f mysql-pvc.yaml
kubectl apply -f mysql-deployment.yaml
kubectl apply -f mysql-service.yaml

# Deploy counter-app
kubectl apply -f counter-app-deployment.yaml
kubectl expose deployment counter-app --type=NodePort --port=80

# Deploy Prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
minikube service prometheus-server-np

# Deploy Grafana
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np
minikube service grafana-server-np

# # Access DB
# winpty kubectl exec -it --stdin --tty mysql-797db4b8d5-2g9gv  -- sh
# mysql -u root -p

# # Retrieve base64 encoded password for Grafana
# kubectl get secret --namespace default grafana -o yaml
